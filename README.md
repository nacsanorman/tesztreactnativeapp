nodejs szükséges a gépen!

Visual Studio Code-ban megnyitottam a workspace-t, termináljába beírom:
- npm install
- expo start

a megjelenő management oldalon én az open in web page-vel tudom nézni, de mivel helyi hálózaton fut, ezért mobilon is meg tudtam nyitni ott az ip címet
ha lenne felrakva pl android studiós vagy valamilyen android emulátor, ott meg lehetne nyitni az alkalmazást